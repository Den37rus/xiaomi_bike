import Swiper, { EffectFade, Pagination, Autoplay, EffectFlip, EffectCoverflow, EffectCards, EffectCreative } from 'swiper';

const inputs = document.querySelectorAll('.input-group__el');
const activeClass = 'input-group__item--active';
const formEl = document.querySelector('#buy-form');

function changeColor(slider) {
  inputs.forEach((el) => {
    if (el.checked) {
      el.parentNode.classList.add(activeClass);
      slider.slideTo(el.value);
    } else {
      el.parentNode.classList.remove(activeClass);
    }
  });
}

function sendForm(ev) {
  ev.preventDefault();
  const formData = new FormData(formEl);
  const formEntries = [];
  for (let el of formData.entries()) {
    formEntries.push(el);
  }
  console.log(Object.fromEntries(formEntries));
  alert('Форма отправлена');
}

window.onload = () => {
  const s1 = new Swiper('.swiper-1', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 30,
    autoplay: {
      delay: 4000,
      pauseOnMouseEnter: true,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.hero__pagination',
      bulletClass: 'hero-pagination__item',
      bulletActiveClass: 'hero-pagination__item--active',
      type: 'bullets',
    },
    modules: [Pagination, Autoplay],
  });

  const s2 = new Swiper('.slider__wrapper', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 30,
    autoplay: {
      delay: 4000,
      pauseOnMouseEnter: true,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.slider__pagination',
      bulletClass: 'pagination__item',
      bulletActiveClass: 'pagination__item--active',
      type: 'bullets',
    },
    modules: [Pagination, Autoplay],
  });

  const s3 = new Swiper('.order__slider', {
    slidesPerView: 1,
    allowTouchMove: false,
    effect: 'creative',
    creativeEffect: {
      prev: {
        translate: [0, 0, -800],
        opacity: 0,
      },
      next: {
        translate: [0, 0, -1600],
        opacity: 1,
      },
    },
    modules: [EffectCreative],
  });

  changeColor(s3);
  inputs.forEach((el) => {
    el.addEventListener('change', changeColor.bind(null, s3));
  });

  document.querySelector('.hero__btn').addEventListener('click', () => {
    window.scrollTo({ top: document.body.offsetHeight, behavior: 'smooth' });
  });
  document.querySelector('.order__btn').addEventListener('click', sendForm);
};
